import React from "react";
import { Form } from "react-bootstrap";

const ProductForm = React.memo(function ProductForm({
  product,
  editData,
  setProduct,
  handleSubmit,
  validated,
}) {

  const onValueChange = (e) => {
    setProduct({ ...product, [e.target.name]: e.target.value });
  };

  return (
    <Form noValidate validated={validated} onSubmit={handleSubmit}>
      <div className="info-widget">
        <div className="row">
          <div className="col-md-6 col-sm-12">
            <div className="form-group card-label">
              <Form.Label>Title *</Form.Label>
              <Form.Control
                className="form-control"
                onChange={onValueChange}
                value={product.title}
                name="title"
                type="text"
                required
              />
              <Form.Control.Feedback type="invalid">
                Title should not ne empty.
              </Form.Control.Feedback>
            </div>
          </div>
          <div className="col-md-6 col-sm-12">
            <div className="form-group card-label">
              <Form.Label>Price *</Form.Label>
              <Form.Control
                className="form-control"
                onChange={onValueChange}
                value={product.price}
                name="price"
                type="number"
                required
              />
              <Form.Control.Feedback type="invalid">
                Price should not ne empty.
              </Form.Control.Feedback>
            </div>
          </div>
        </div>
        <div className="submit-section proceed-btn text-end">
          <button type="submit" className="btn btn-primary submit-btn">
            {editData ? "Update Product" : "Add Product"}
          </button>
        </div>
      </div>
    </Form>
  );
});

export default ProductForm;
