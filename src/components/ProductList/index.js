import React, { useContext, useEffect, useState } from "react";
import { Button, Modal } from "react-bootstrap";
import { GlobalContext } from "../../context/store";
import Pagination from "../common/Pagination";
import ProductForm from "../common/productForm";

function ProductList() {
  let PageSize = 10;
  const [show, setShow] = useState(false);
  const [validated, setValidated] = useState(false);
  const [product, setProduct] = useState({
    title: "",
    price: "",
  });
  const [editData, setEditData] = useState();
  const [currentPage, setCurrentPage] = useState(1);
  const [deleteId, setDeleteId] = useState();

  //imported from context
  const {
    fetchProduct,
    loading,
    productList,
    addProduct,
    editProduct,
    deleteProduct,
  } = useContext(GlobalContext);

  const handleShow = () => setShow(true);
  const handleClose = () => {
    setShow(false);
    setDeleteId();
  };

  //edit and add product submit form
  const handleSubmit = async (event) => {
    const form = event.currentTarget;
    event.preventDefault();
    event.stopPropagation();
    setValidated(true);
    if (!form.checkValidity()) {
      return;
    }
    if (show) {
      addProduct(product);
    } else {
      editProduct({ ...editData, ...product });
    }
    handleClose();
    setProduct({
      title: "",
      price: "",
    });
    setEditData();
    setValidated(false);
  };

  //fetch data
  useEffect(() => {
    let skip = (currentPage - 1) * PageSize;
    fetchProduct(skip);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [currentPage]);

  //add product
  const handleAddProduct = (e) => {
    setEditData();
    setProduct({
      title: "",
      price: "",
    });
    e.stopPropagation();
    handleShow();
  };

  //update product
  const handleEditProduct = (item) => {
    setEditData(item);
    setProduct({ title: item.title, price: item.price });
  };

  //delete product
  const handleDelete = () => {
    deleteProduct(deleteId);
    handleClose();
  };

  //open delete product modal
  const openDeleteModal = (id) => {
    setDeleteId(id);
    handleShow();
  };

  return (
    <>
      {loading ? (
        <div className="main-loader-div">
          <span className="loader"></span>
          <p> loading...</p>
        </div>
      ) : (
        <div>
          {productList?.products?.data?.length ? (
            <button
              className="btn btn-success add-pro-btn"
              onClick={(e) => handleAddProduct(e)}
            >
              Add Product
            </button>
          ) : null}
          <div></div>
          <div className="product-list">
            {productList?.products?.data?.length
              ? productList?.products?.data?.map((item, idx) => {
                  return (
                    <div className="product-inner-box" key={idx}>
                      <div className="product-items-img">
                        <img
                          height={300}
                          width={400}
                          src={
                            item?.thumbnail
                              ? item?.thumbnail
                              : "https://placebear.com/640/360"
                          }
                          alt="Thunbnail"
                        />
                      </div>
                      <>
                        {editData && editData?.id === item?.id ? (
                          <>
                            <ProductForm
                              product={product}
                              setProduct={setProduct}
                              handleSubmit={handleSubmit}
                              validated={validated}
                              editData={true}
                            />
                          </>
                        ) : (
                          <>
                            <h3>{item.title}</h3>
                            <p>{item.price}</p>
                            <div className="buttone-group">
                              <button
                                className="btn btn-primary "
                                onClick={() => handleEditProduct(item)}
                              >
                                Edit
                              </button>
                              <button
                                className="btn btn-light "
                                onClick={() => openDeleteModal(item.id)}
                              >
                                Delete
                              </button>
                            </div>
                          </>
                        )}
                      </>
                    </div>
                  );
                })
              : "No More Product data available on this page"}
          </div>
          <Pagination
            className="pagination-bar"
            currentPage={currentPage}
            totalCount={productList?.products?.total}
            pageSize={PageSize}
            onPageChange={(page) => setCurrentPage(page)}
          />
          <Modal
            show={show && !deleteId}
            onHide={handleClose}
            animation={false}
          >
            <Modal.Header closeButton>
              <Modal.Title>Add Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <ProductForm
                product={product}
                setProduct={setProduct}
                handleSubmit={handleSubmit}
                validated={validated}
              />
            </Modal.Body>
          </Modal>
          <Modal show={show && deleteId} onHide={handleClose} animation={false}>
            <Modal.Header closeButton>
              <Modal.Title>Delete Product</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              <span className="delete-text">
                {" "}
                Are you sure you want to delete this Product!{" "}
              </span>
            </Modal.Body>
            <Modal.Footer>
              <Button variant="light" onClick={handleClose}>
                Cancel
              </Button>
              <Button variant="danger" onClick={handleDelete}>
                Yes
              </Button>
            </Modal.Footer>
          </Modal>
        </div>
      )}
    </>
  );
}

export default ProductList;
