import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import 'bootstrap/dist/js/bootstrap.bundle.js';
import { Store } from "./context/store";
import ProductList from "./components/ProductList";
import "./index.scss";

function App() {
  return (
    <Store>
      <ProductList />
    </Store>
  );
}

export default App;
