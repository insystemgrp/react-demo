import * as actions from "./actions";

// FETCH_PRODUCT
const getProductData = (state, payload) => {
  return {
    ...state,
    products: {
      ...state.products,
      data: payload?.products,
      total: payload?.total,
    },
  };
};

// ADD_PRODUCT
const addProductData = (state, payload) => {
  return {
    ...state,
    products: {
      ...state.products,
      data: [...state.products.data, payload],
    },
  };
};

// EDIT_PRODUCT
const editProductData = (state, payload) => {
  return {
    ...state,
    products: {
      ...state.products,
      data: state.products.data.map((item) =>
        item.id === payload.id ? payload : item
      ),
    },
  };
};

// DELETE_PRODUCT
const deleteProductData = (state, payload) => {
  return {
    ...state,
    products: {
      ...state.products,
      data: state.products.data.filter((item) => item.id !== payload),
    },
  };
};

export const ProductReducer = (state, action) => {
  const { type, payload } = action;
  switch (type) {
    // FETCH_PRODUCT
    case actions.FETCH_PRODUCT:
      return getProductData(state, payload);

    // ADD_PRODUCT
    case actions.ADD_PRODUCT:
      return addProductData(state, payload);

    // EDIT_PRODUCT
    case actions.EDIT_PRODUCT:
      return editProductData(state, payload);

    // DELETE_PRODUCT
    case actions.DELETE_PRODUCT:
      return deleteProductData(state, payload);

    // ERROR_PRODUCT
    case actions.ERROR_PRODUCT:
      return {
        ...state,
      };

    default:
      return state;
  }
};
