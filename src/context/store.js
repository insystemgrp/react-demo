import React, { createContext, useId, useReducer, useState } from "react";
import {
  ADD_PRODUCT,
  DELETE_PRODUCT,
  EDIT_PRODUCT,
  ERROR_PRODUCT,
  FETCH_PRODUCT,
} from "./actions";
import { ProductReducer } from "./reducer";
import { initialState } from "./state";

export const GlobalContext = createContext(initialState);

export const Store = (props) => {
  const [productList, dispatch] = useReducer(ProductReducer, initialState);
  const [loading, setLoading] = useState(false);
  const id = useId();

  // FETCH_PRODUCT
  const fetchProduct = (skip) => {
    setLoading(true);
    fetch(`https://dummyjson.com/products?limit=10&skip=${skip}`)
      .then((res) => res.json())
      .then((data) => {
        dispatch({
          type: FETCH_PRODUCT,
          payload: data,
        });
        setLoading(false);
      })
      .catch((error) => {
        dispatch({
          type: ERROR_PRODUCT,
        });
        setLoading(false);
      });
  };

  // ADD_PRODUCT
  const addProduct = (payload) => {
    setLoading(true);
    fetch("https://dummyjson.com/products/add", {
      method: "POST",
      body: JSON.stringify(payload),
      headers: {
        "Content-type": "application/json",
      },
    })
      .then((data) => {
        dispatch({
          type: ADD_PRODUCT,
          payload: { ...payload, id },
        });
        setLoading(false);
      })
      .catch((error) => {
        dispatch({
          type: ERROR_PRODUCT,
        });
        setLoading(false);
      });
  };

  // EDIT_PRODUCT
  const editProduct = (payload) => {
    setLoading(true);
    fetch(`https://dummyjson.com/products/${payload.id}`, {
      method: "PUT",
      headers: { "Content-Type": "application/json" },
      body: JSON.stringify({ title: payload.id, price: payload.price }),
    })
      .then((data) => {
        dispatch({
          type: EDIT_PRODUCT,
          payload,
        });
        setLoading(false);
      })
      .catch((error) => {
        dispatch({
          type: ERROR_PRODUCT,
        });
        setLoading(false);
      });
  };

  // DELETE_PRODUCT
  const deleteProduct = async (id) => {
    setLoading(true);
    fetch(`https://dummyjson.com/products/${id}`, {
      method: "DELETE",
    })
      .then((data) => {
        dispatch({
          type: DELETE_PRODUCT,
          payload: id,
        });
        setLoading(false);
      })
      .catch((error) => {
        dispatch({
          type: ERROR_PRODUCT,
        });
        setLoading(false);
      });
  };

  return (
    <GlobalContext.Provider
      value={{
        productList,
        dispatch,
        loading,
        setLoading,
        fetchProduct,
        addProduct,
        editProduct,
        deleteProduct,
      }}
    >
      {props.children}
    </GlobalContext.Provider>
  );
};
